#### 1. DataMining.ipynb - Jupyter Notebook with a source code in Python
#### 2. Sankey diagram.R - source code in RStudio with the Sankey Diagram
#### 3. Rplot.png and Sankey diagram.html - the actual Sankey diagram 
#### 4. asylum_seekers.csv and time_series.csv - data files that were used in my project
